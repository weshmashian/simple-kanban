;;; simple-kanban.el --- Simplistic implementation of kanban board for Org  -*- lexical-binding: t -*-

;; Copyright (C) 2017 Marko Crnić

;; Author: Marko Crnić (weshmashian)
;; Homepage: https://gitlab.com/weshmashian/simple-kanban
;; Version: 0.1.1
;; Package-Requires: (dash s)

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Package adds a new dynamic block type named 'kanban'. When evaluated, it collects org headings,
;; restricted by `:match' and `:scope' parameters. Entries can be further filtered by setting
;; `:headers' to list of wanted TODO states, and `:exclude' to a list of ignoreable TODO states.
;;
;; See `org-dblock-write:kanban' for description of available parameters.
;;
;; For example, to get a table with all TODO states present in buffer while ignoring some states:
;; #+BEGIN: kanban :exclude ("CANCELLED" "DONE") :scope file
;; #+END

;;; Code:
(require 'dash)
(require 's)

(defun simple-kanban-get-entries (&optional match scope skip)
  "Pass MATCH, SCOPE and SKIP to `org-map-entries' and return a list of properties for each element."
  (org-map-entries 'org-entry-properties match scope skip))

(defun simple-kanban-create-link (entry)
  "Create a Org link from ENTRY.

Properties are used in following order:
 - ID if `org-id' is used
 - CUSTOM_ID
 - ITEM

Returns string representation of Org link."
  (let ((file (cdr (assoc "FILE" entry)))
	(id (cdr (assoc "ID" entry)))
	(custom_id (cdr (assoc "CUSTOM_ID" entry)))
	(item (cdr (assoc "ITEM" entry))))
    (org-make-link-string
     (if (and (featurep 'org-id) id)
	 (concat "id:" id)
       (concat "file:" file "::"
	       (or
		(when custom_id (concat "#" custom_id))
		(org-make-org-heading-search-string item))))
     (s-replace-all '(("" . "")) item))))

(defun simple-kanban-create-table (headers &optional excludes)
  "Create table and fill first row of each column with HEADERS."
  (let ((hdrs (-difference headers excludes)))
    (org-table-create (concat (number-to-string (length hdrs)) "x1"))
    (simple-kanban-insert-row hdrs)
    (org-table-hline-and-move)
    (org-table-goto-line 1)
    (org-table-kill-row)))

(defun simple-kanban-insert-row (entries)
  "Append row and fill cells with ENTRIES."
  (when (org-at-table-p)
    (goto-char (org-table-end))
    (forward-line -1)
    (org-table-insert-row 'below)
    (let ((column (org-table-current-column)))
      (--each entries
      	(org-table-put (org-table-current-line) column it)
      	(setq column (1+ column))))
    (org-table-align)))

(defun simple-kanban-fill-columns (entries)
  "Fill table columns using ENTRIES, filtering by value of column's first cell."
  (when (org-at-table-p)
    (org-table-analyze)
    (org-table-goto-line 2)
    (let ((col 1)
	  (head))
      (while (<= col org-table-current-ncol)
	(--each entries
	  (when (string= (cdr (assoc "TODO" it)) (org-table-get 1 col))
	    (insert (simple-kanban-create-link it))
	    (org-table-next-row)))
	(setq col (1+ col))
	(org-table-goto-line 2)
	(org-table-goto-column col)))))


(defun simple-kanban-add-format (format)
  "Insert format row at the top of the table using FORMAT list.

FORMAT is a list of org-table formats, one per column."
  (when (and format (org-at-table-p))
    (org-table-goto-line 1)
    (org-table-insert-row)
    (--each format
      (when it
	(insert it))
      (org-table-next-field))))

;;;###autoload
(defun org-dblock-write:kanban (params)
  "Create a kanban board according to PARAMS.

Available params are as follows:

headers  List of headers to include in search
exclude  List of headers to exclude from search
match, scope, skip
         These variables are passed directly to `org-map-entries'
format   List of org-table column formats"
  (simple-kanban-create-table (or (plist-get params :headers) org-todo-keywords-1) (plist-get params :exclude))
  (simple-kanban-fill-columns
   (simple-kanban-get-entries
    (plist-get params :match)
    (plist-get params :scope)
    (plist-get params :skip)))
  (simple-kanban-add-format (plist-get params :format)))


(provide 'simple-kanban)
;;; simple-kanban.el ends here
